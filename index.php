<?php
	
	/**
	 * Plugin Name: ROKIB  Crud Management
	 * Plugin URI: http://www.frndzit.com
	 * Description: Cru Management Plugin.
	 * Version: 1.0.0
	 * Author: ROkib
	 * Author URI: https://www.facebook.com/rokibulislam.rokib.71
	 * Text Domain: Optional. Plugin's text domain for localization. Example: mytextdomain
	 * Domain Path: Optional. Plugin's relative directory path to .mo files. Example: /locale/
	 * Network: Optional. Whether the plugin can only be activated network wide. Example: true
	 * License: GPL2
	 */




	$table_name = $wpdb->prefix . 'rok_users';
	$sql="SELECT * FROM $table_name";
	$myrows = $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);

	global $jal_db_version;
	
	$jal_db_version = '1.0';

	function rok_crud_activate() {

	    require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
	   
	    global $wpdb;
	   
	    global $jal_db_version;


	    $dbtable_name = $wpdb->prefix .  'rok_users';

	    $charset_collate = '';

	    if ( ! empty( $wpdb->charset ) ) {
	      $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
	    }

	    if ( ! empty( $wpdb->collate ) ) {
	      $charset_collate .= " COLLATE {$wpdb->collate}";
	    }

	    $sql="CREATE TABLE IF NOT EXISTS `$dbtable_name` (
		         `id` int(11) NOT NULL AUTO_INCREMENT,
	 			 `name` varchar(255) NOT NULL,
	  			  `email` varchar(255) NOT NULL,
	  			  PRIMARY KEY (`id`)
	        ) ENGINE=MyISAM DEFAULT CHARSET=latin1; ";

	    dbDelta( $sql );

	    add_option( 'jal_db_version', $jal_db_version );
	}

	register_activation_hook( __FILE__, 'rok_crud_activate' );

	function rok_crud_deactivate() {
	     
	     global $wpdb;
	     $dbtable_name = $wpdb->prefix .  'rok_users';
	     $sql = "DROP TABLE IF EXISTS $dbtable_name;";
	     $wpdb->query($sql);
	     delete_option("jal_db_version");
	}

	register_deactivation_hook( __FILE__, 'rok_crud_deactivate' );

	function register_admin_menu(){

		add_menu_page( 'custom menu title', 'custom menu', 'manage_options', 'custompage', 'my_custom_menu_page'); 

	}

	add_action('admin_menu','register_admin_menu');

	function plugin_scripts(){

		wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => home_url(),
        'loadingmessage' => __('Sending user info, please wait...')
    ));
		wp_register_style( 'ui-styles',  plugin_dir_url( __FILE__ ) . 'assets/css/jquery-ui.min.css' );
	    wp_enqueue_style( 'ui-styles' );
	    wp_register_style('ui-theme-styles',  plugin_dir_url( __FILE__ ) . 'assets/css/jquery-ui.theme.min.css' );
	    wp_enqueue_style( 'ui-theme-styles' );
		wp_register_style( 'crud-styles',  plugin_dir_url( __FILE__ ) . 'assets/css/crud-styles.css' );
	    wp_enqueue_style( 'crud-styles' );
	    wp_register_script('ui-js',plugin_dir_url( __FILE__ ) . 'assets/js/jquery-ui.min.js');
	    wp_enqueue_script('ui-js');
	    wp_register_script('crud-js',plugin_dir_url( __FILE__ ) . 'assets/js/crud-js.js');
	    wp_enqueue_script('crud-js');


		
	}

	add_action('admin_enqueue_scripts','plugin_scripts');



	function my_custom_menu_page(){
		global $wpdb;
		include 'templates/manage_crud.php'; 
	}




	function edit_action(){

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
 		global $wpdb;
 		$user_id=$_POST['user_id'];
 		$table_name = $wpdb->prefix .'rok_users';
 		$sql="SELECT * FROM $table_name where id='$user_id'";
 		$myrows = $wpdb->get_results( $sql );
		$mystr="";

		foreach ( $myrows as $myrow ) {
					

					$mystr.=$myrow->name;
					$mystr.="-&-";
					$mystr.=$myrow->email;
					$mystr.="-&-";
					$mystr.=$myrow->id;

		}



		echo $mystr;
		//echo json_encode($myrows);
		exit;	

	}

	add_action( 'wp_ajax_edit_action', 'edit_action' );
	add_action('wp_ajax_nopriv_edit_action','edit_action');



	function delete_action(){
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
		//require_once(dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/wp-config.php");
 		global $wpdb;
 		$user_id=$_POST['user_id'];
 		$table_name = $wpdb->prefix . 'rok_users';
 		$sql="DELETE FROM $table_name where id='$user_id'";
 		$wpdb->query($sql);

 		echo "hello world";

     	exit;

	}

	add_action( 'wp_ajax_delete_action', 'delete_action' );

	add_action('wp_ajax_nopriv_delete_action','delete_action');



		$name=$_POST['name'];
        $email=$_POST['email'];

	function save_action(){

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
	//	require_once(dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/wp-config.php");
 		
		$name=$_POST['name'];
        $email=$_POST['email'];
        $table_name = $wpdb->prefix . 'rok_users';
        if(isset($_POST['user_id']) && $_POST['user_id']!=="0"){
        	$user_id=$_POST['user_id'];
        	$sql2="UPDATE $table_name SET  name='$name',email='$email' WHERE id='$user_id'";
        }else{
    		//$sql2="INSERT INTO $table_name (`name`, `email`) VALUES ('$name', '$email')";	
    		$wpdb->insert( 
	$table_name, 
	array( 
		'name' => $name, 
		'email' => $email 
	), 
	array( 
		'%s', 
		'%s' 
	) 
);
    		     	//$wpdb->query($sql2);
     	

     	$lastid = $wpdb->insert_id;

     	$sql="SELECT * FROM $table_name where id='$lastid'";

     	$myrows = $wpdb->get_results( $sql );



     	/*$rokstr="";
     	foreach ($myrow as $myrows ) {
	     	var_dump($myrow);
	     	$rokstr .="<tr>";
	     	$rokstr .="<td>" . $myrow['id']. "</td>";
			$rokstr .="<td>" . $myrow['name'] . "</td>";
			$rokstr .="<td>" . $myrow['email'] . "</td>";
			$rokstr .="<td><a href=\" \" class=\"edit_crud\" user-id=". $myrow['id'] ">Edit</a>"; 
			$rokstr .="<a href=\" \" class=\"delete_crud\" user-id=" . $myrow['id'] ." >delete</a></td>";
			$rokstr .="</tr>";
		}


*/
     	
     		$rokstr="";

		//foreach ( $myrow as $myrows ) {
				
			$rokstr .="<tr>";
	     	$rokstr .="<td>" . $myrows[0]->id. "</td>";
			$rokstr .="<td>" . $myrows[0]->name . "</td>";
			$rokstr .="<td>" . $myrows[0]->email . "</td>";
			$rokstr .="<td><a href=\" \" class=\"edit_crud\" user-id=". $myrows[0]->id .">Edit</a>";
			$rokstr .="<td><a href=\" \" class=\"delete_crud\" user-id=". $myrows[0]->id .">Delete</a>";
			$rokstr .="</tr>";     	

     	$lastid = $wpdb->insert_id;

     	$sql="SELECT * FROM $table_name where id='$lastid'";

     	$myrows = $wpdb->get_results( $sql );



     	/*$rokstr="";
     	foreach ($myrow as $myrows ) {
	     	var_dump($myrow);
	     	$rokstr .="<tr>";
	     	$rokstr .="<td>" . $myrow['id']. "</td>";
			$rokstr .="<td>" . $myrow['name'] . "</td>";
			$rokstr .="<td>" . $myrow['email'] . "</td>";
			$rokstr .="<td><a href=\" \" class=\"edit_crud\" user-id=". $myrow['id'] ">Edit</a>"; 
			$rokstr .="<a href=\" \" class=\"delete_crud\" user-id=" . $myrow['id'] ." >delete</a></td>";
			$rokstr .="</tr>";
		}


*/
     	
     		$rokstr="";

		//foreach ( $myrow as $myrows ) {
				
			$rokstr .="<tr>";
	     	$rokstr .="<td>" . $myrows[0]->id. "</td>";
			$rokstr .="<td>" . $myrows[0]->name . "</td>";
			$rokstr .="<td>" . $myrows[0]->email . "</td>";
			$rokstr .="<td><a href=\" \" class=\"edit_crud\" user-id=". $myrows[0]->id .">Edit</a>";
			$rokstr .="<td><a href=\" \" class=\"delete_crud\" user-id=". $myrows[0]->id .">Delete</a>";
			$rokstr .="</tr>";
	//	}


			echo $rokstr;
	//	}


    	}



		exit;

/*	     echo json_encode($mystr);

     	exit;*/
	}

	add_action( 'wp_ajax_save_action', 'save_action' );

	add_action('wp_ajax_nopriv_save_action','save_action');


	function deleteall_action(){

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
		
		global $wpdb;

		$table_name = $wpdb->prefix . 'rok_users';

		$ids=implode(',',$_POST['ids']);
			
        $sql="DELETE FROM $table_name where id in(" . $ids .") ";

        $wpdb->query($sql);

        echo $ids;

        exit;
	}


	add_action('wp_ajax_deleteall_action','deleteall_action');
	add_action('wp_ajax_nopriv_deleteall_action','deleteall_action');

	function search_action(){

		$search=$_POST['search'];

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;
		
		$table_name = $wpdb->prefix . 'rok_users ';
		
		$sql="SELECT * FROM $table_name WHERE name LIKE '%$search%'";

	
     	
     	$myrows = $wpdb->get_results( $sql );


     	$rokstr="";

     	foreach ( $myrows as $myrow ) {
				
			$rokstr .="<tr>";
	     	$rokstr .="<td>" . $myrow->id . "</td>";
			$rokstr .="<td>" . $myrow->name . "</td>";
			$rokstr .="<td>" . $myrow->email . "</td>";
			$rokstr .="<td><a href=\" \" class=\"edit_crud\" user-id=". $myrow->id.">Edit</a>";
			$rokstr .="<td><a href=\" \" class=\"delete_crud\" user-id=". $myrow->id .">Delete</a>";
			$rokstr .="</tr>";
		}

		echo $rokstr;


		exit;

	}

	add_action('wp_ajax_search_action','search_action');
	add_action('wp_ajax_nopriv_search_action','search_action');



	$data = array(
     '0' => array('Name'=> 'user1', 'Status' =>'complete', 'Priority'=>'Low', 'Salary'=>'001'),
     '1' => array('Name'=> 'user2', 'Status' =>'inprogress', 'Priority'=>'Low', 'Salary'=>'111'),
     '2' => array('Name'=> 'user3', 'Status' =>'hold', 'Priority'=>'Low', 'Salary'=>'333'),
     '3' => array('Name'=> 'user4', 'Status' =>'pending', 'Priority'=>'Low', 'Salary'=>'444'),
     '4' => array('Name'=> 'user5', 'Status' =>'pending', 'Priority'=>'Low', 'Salary'=>'777'),
     '5' => array('Name'=> 'user6', 'Status' =>'pending', 'Priority'=>'Low', 'Salary'=>'777')
    );
	if(isset($_POST["ExportType"])){
    
	    switch($_POST["ExportType"]){
	        case "export-to-excel" :
	            // Submission from
	            $filename = $_POST["ExportType"] . ".xls";      
	          //  header("Content-Type: application/vnd.ms-excel");
	           // header("Content-Disposition: attachment; filename=\"$filename\"");
	            ExportFile($myrows);
	            //$_POST["ExportType"] = '';
	            exit();
	        case "export-to-csv" :
	            // Submission from
	            $filename = $_POST["ExportType"] . ".csv";      
	            //header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	            //header("Content-type: text/csv");
	             header('Content-Type: application/csv');
	           header("Content-Disposition: attachment; filename=\"$filename\"");
	            //header("Expires: 0");

	            ExportCSVFile($myrows);
	            //$_POST["ExportType"] = '';
	            exit();
	        case "export-to-pdf" :
	        	exporttopdf();
	         exit();
	        default :
	            die("Unknown action : ".$_POST["action"]);
	            break;
	    }
	}
 
		function ExportCSVFile($records) {
		    // create a file pointer connected to the output stream
		    $fh = fopen( 'php://output', 'w' );
		    //$fh = fopen("tmp.csv", "w");
		    $heading = false;
		        if(!empty($records))
		          foreach($records as $row) {
		            if(!$heading) {
		              // output the column headings
		              fputcsv($fh, array_keys($row));
		              $heading = true;
		            }
		            // loop over the rows, outputting them
		             fputcsv($fh, array_values($row));
		              
		          }
		          fclose($fh);
		}
 
		function ExportFile($records) {
		    $heading = false;
		    if(!empty($records))
		      foreach($records as $row) {
		        if(!$heading) {
		          // display field/column names as a first row
		         echo implode("\t", array_keys($row)) . "\n";
		          $heading = true;
		        }
		        echo implode("\t", array_values($row)) . "\n";
		      }
		    exit;
		}

	function exporttopdf(){
		 global $wpdb;
		$table_name = $wpdb->prefix . 'rok_users';
		$sql="SELECT * FROM $table_name";

		//require_once('tcpdf/config/lang/eng.php');
		require_once('tcpdf/tcpdf.php');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Dario Balas');
		$pdf->SetTitle('PDF');
		$pdf->SetSubject('PDF');
		$pdf->SetKeywords('PDF');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->setLanguageArray($l);
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('dejavusans', '', 14, '', true);
		$pdf->AddPage();
		$tbl_header = '<table border="1">';
		$tbl_footer = '</table>';
		$tbl ='';
		
		$myrows = $wpdb->get_results("SELECT * FROM $table_name");
		foreach ($myrows as $row) {

			$tbl .= '<tr> ';
			$tbl .='<td>' .$row->id  .'</td>';
			$tbl .='<td>'. $row->name . '</td>';
			$tbl .='<td>'. $row->email .'</td>';
			$tbl .= '</tr>';
		}

		//$tbl='';
 		
		$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, false, false, false, '');
		$pdf->Output('', 'I');
	}