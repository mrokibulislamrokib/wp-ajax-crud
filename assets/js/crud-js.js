jQuery(document).ready(function($) {

	 	$('#export-menu li').bind("click", function() {
            var target = $(this).attr('id');
            switch(target) {
                case 'export-to-excel' :
	                $('#hidden-type').val(target);
	                //alert($('#hidden-type').val());
	                $('#export-form').submit();
	                $('#hidden-type').val('');
                break
                case 'export-to-csv' :
	                $('#hidden-type').val(target);
	                //alert($('#hidden-type').val());
	                $('#export-form').submit();
	                $('#hidden-type').val('');
                break

                case 'export-to-pdf' :
                	$('#hidden-type').val(target);
	                //alert($('#hidden-type').val());
	                $('#export-form').submit();
	                $('#hidden-type').val('');
                break;
            }
        });


	 $('.btnSearch').click(function(event) {

	 	event.preventDefault();

	 	var search=$('#search').val();

	 	console.log(search);
	 	
	 	jQuery.ajax({
		      type: "POST",
		      url: ajaxurl, 
		      data: { action:'search_action','search':search}, 
		      success: function(response){
		      	console.log(response);
		      },
		      error: function(MLHttpRequest, textStatus, errorThrown){  
		         alert("There was an error: " + errorThrown);  
		      },
		      timeout: 60000
		});


	 });

	 $("#ckbCheckAll").click(function () {
           $(".checkBoxClass").prop('checked', true);
     });

	  $("#UNckbCheckAll").click(function () {
           $(".checkBoxClass").prop('checked', false);
     });

     

     $('#deleteall').click(function(event){

     	event.preventDefault();

     	 var checkValues = $('input[name=case]:checked').map(function()
            {
                return $(this).val();
            }).get();


         console.log(checkValues);


     	jQuery.ajax({
		      type: "POST",
		      url: ajaxurl, 
		      data: { action:'deleteall_action',ids: checkValues}, 
		      success: function(response){
		      		//console.log(response);
					
					var arr = response.split(',');

					console.log(arr);

					for (var i = 0; i < arr.length; i++) {
						
						$('#crud_table > tbody tr#' + arr[i]).remove();
					}
				  
		      },
		      error: function(MLHttpRequest, textStatus, errorThrown){  
		         alert("There was an error: " + errorThrown);  
		      },
		      timeout: 60000
		});  

     });

     /*$('.checkbox').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });*/


	dialog = $( "#dialog-form" ).dialog({
      	autoOpen: false,
      	height: 600,
      	width: 600,
      	modal: true,
      	close: function() {
      	
      	}
    });

	$(".close").click(function(event) {
			
			//$(this).parent().fadeOut('slow');
    });

	$( "#add_crud" ).on('click',function(event) {
		
		event.preventDefault();
		
		dialog.dialog( "open" );
	});


	$( ".edit_crud" ).on('click',function(event) {

		

		
		event.preventDefault();

		var user_id=$(this).attr("user-id");

		var row_id = $(this).closest("tr").attr("id");

		jQuery.ajax({
		      type: "POST",
		      url: ajaxurl, 
		      data: { action:'edit_action','user_id':user_id,'row_id':row_id}, 
		      success: function(response){
		      		console.log(response);
		        	//alert("Got this from the server: " + response);
		         	dialog.dialog("open");
					var arr = response.split('-&-');
					$('#name').val(arr[0]);
				  	$('#email').val(arr[1]);
				  	$('#user_id').val(arr[2]);
		      },
		      error: function(MLHttpRequest, textStatus, errorThrown){  
		         alert("There was an error: " + errorThrown);  
		      },
		      timeout: 60000
		});  


		/*$.ajax({
			url: ajaxurl,
			action:'edit_action',
			type: 'POST',
			dataType:'html',
			data: {
				'user_id':user_id
			},
		})
		.done(function(data) {
			console.log(data);
			dialog.dialog("open");
			var arr = data.split('-&-');
			$('#name').val(arr[0]);
		  	$('#email').val(arr[1]);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});*/
	});


	


	$(".delete_crud").click(function(event) {

		event.preventDefault();

		var  user_id=$(this).attr("user-id");

		var $ele = $(this).parent().parent();

		jQuery.ajax({
		      type: "POST",
		      url: ajaxurl, 
		      data: { action:'delete_action','user_id':user_id}, 
		      success: function(response){ 
		      	 $ele.fadeOut().remove();
		        // alert("Got this from the server: " + response);
		      },
		      error: function(MLHttpRequest, textStatus, errorThrown){  
		         alert("There was an error: " + errorThrown);  
		      },
		      timeout: 60000
		});  

	/*	$.ajax({
			url:ajaxurl,
			action:'delete_action',
			type: 'POST',
			dataType:'html',
			data: {
				'user_id':user_id
			},
		})
		.done(function(data) {
			console.log(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});*/

	});


	$('#sbmt_btn').click(function(event) {
		
		event.preventDefault();

		var name=$('#name').val();
		var email=$('#email').val();

		var user_id=$('#user_id').val();


		jQuery.ajax({
		      type: "POST",
		      url: ajaxurl, 
		      dataType:'html',
		      data: { action:'save_action','name':name,'email':email,'user_id':user_id}, 
		      success: function(response){ 
		      	console.log(response);
		  		jQuery('#crud_table  tbody').append(response)
		     // 	console.log(response.length);
		      /*	var res=JSON.stringify(response);
		      	console.log(res);*/
		         
				//jQuery('#crud_table  tbody').empty();

				/*jQuery.each(response, function(index, value) {
				   console.log('element at index ' + index + ' is ' + value);
				});*/
/*
				for (var i=0; i < res.length; i++){


				}*/

				/*var body="";

				$.each(response, function(i) {

				  body +="<tr>";	
				  
				  $.each(response[i], function(key,val) {
				     body +="<td>" + val + "</td>";
				  });

				   body +="</tr>";
				});

				jQuery('#crud_table  tbody').append(body);*/

				/*var body="";

				for (var i=0; i<response.length; i++){
				    
					body +="<tr>";
				   
				    for (var name in response[i]) {
        				body +="<td>" + response[i][name] + "</td>";
				    }

				    body +="</tr>";
				}

				jQuery('#crud_table  tbody').append(body);*/
				
				
		      },
		      error: function(MLHttpRequest, textStatus, errorThrown){  
		         alert("There was an error: " + errorThrown);  
		      },
		      timeout: 60000
		});  

		/*jQuery.ajax({
			url: ajaxurl,
			
			type: 'POST',
			dataType:'html',
			data: {
				action:'save_action',
				'name':name,
				'email':email
			},
		})
		.done(function(data) {
			console.log(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});*/
	});
	
});